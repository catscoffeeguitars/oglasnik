﻿$(function () {
    $('[data-toggle="popover"]').popover({
        trigger: 'hover',
        placement: 'bottom',
        html: true,
        content: function () {
            return $(this).closest('.panel.oglas').data('oglas').oglas.opis.replace(/\r\n/g, '<br>');
        }
    });

    updatePageOglasi();

    $('#splitBy').on('change', function () {
        updatePageOglasi();
    });

    $('.body-content').on('click', 'nav .pagination li.pagenum a', function (e) {
        e.preventDefault();
        updatePageOglasi($(this).text());
        return false;
    });

    $('.body-content').on('click', 'nav .pagination li:not(.pagenum) a', function (e) {
        e.preventDefault();
        if ($(this).closest('li').hasClass('disabled')) return false;
        var dir = $(this).closest('li').attr('id');
        var current = parseInt($(this).closest('.pagination').find('li.pagenum.active').removeClass('active').find('a').text());
        switch (dir) {
            case 'next':
                updatePageOglasi(current + 1);
                break;
            case 'prev':
                updatePageOglasi(current - 1);
                break;
        }
        return false;
    });

    $('.filters :checkbox').on('change', function () { updatePageOglasi(); return false; });
    $('#reset').on('click', function () {
        $('.filters :checkbox').prop('checked', true);
        updatePageOglasi();
        return false;
    });

});

function updatePageOglasi(toPage) {
    $('nav .pagination li.pagenum').remove();
    var toPage = typeof toPage === 'undefined' ? 1 : parseInt(toPage);
    var splitBy = typeof $('#splitBy').val() === 'undefined' ? 3 : parseInt($('#splitBy').val());

    var filters = {}, filterKeys = [];

    $('.filters').each(function () {
        var filterFor = $(this).closest('.filters').data('filter_for');
        filterKeys.push(filterFor);
        var values = [];
        $(this).find(':input[type=checkbox]:checked').each(function () {
            values.push($(this).closest('label').text());
        });
        filters[filterFor] = values;
    });

    $('.oglas').each(function () {
        var oglas = $(this).data('oglas'), display = true;
        filterKeys.forEach(function (val) {
            if (filters[val].indexOf(oglas[val]) >= 0)
                display = display && true;
            else
                display = display && false;
        });

        $(this).attr('data-filtered_in', display);
    });

    var oglasIndex = 0, max = $('.oglas[data-filtered_in=true]').length;
    $('#crit_count').text(max);
    for (var i = 0, num_pages = Math.ceil(max / splitBy) ; i < num_pages; i++) {
        for (var j = 0; j < splitBy; j++) {
            $('.oglas[data-filtered_in=true]').eq(oglasIndex).data('page', i + 1);
            oglasIndex++;
        }
        $('<li class=pagenum><a href="#">' + (i + 1) + '</a></li>').insertBefore('nav .pagination #next');
    }
    $('nav .pagination li.pagenum').eq(toPage - 1).addClass('active');

    $('.oglas').each(function () {
        if ($(this).data('page') == $('nav .pagination li.active a').text() && $(this).attr('data-filtered_in') == 'true') {
            $(this).show();
        } else {
            $(this).hide();
        }
    });

    if ($('nav .pagination li.pagenum:first').hasClass('active')) {
        $('nav .pagination #prev').addClass('disabled');
    } else {
        $('nav .pagination #prev').removeClass('disabled');
    }
    if ($('nav .pagination li.pagenum:last').hasClass('active')) {
        $('nav .pagination #next').addClass('disabled');
    } else {
        $('nav .pagination #next').removeClass('disabled');
    }
}
