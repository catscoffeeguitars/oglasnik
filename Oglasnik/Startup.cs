﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Oglasnik.Startup))]
namespace Oglasnik
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
