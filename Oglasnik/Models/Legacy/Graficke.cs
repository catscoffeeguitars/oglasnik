﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Oglasnik.Models {
    [Table("graficke")]
    public class Graficke : KomponentaBase {

        [Display(Name = "Memorija")]
        public string memorija { get; set; }

        [Display(Name = "Slot")]
        public string slot { get; set; }

        [Display(Name = "Frekvencija memorije")]
        public string frek_memorije { get; set; }

        [Display(Name = "Frekvencija jezgre")]
        public string frek_jezgre { get; set; }

        [Display(Name = "Potrošnja")]
        public string potrosnja { get; set; }

    }
}