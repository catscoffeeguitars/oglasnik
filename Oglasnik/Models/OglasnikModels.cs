﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MySql.Data.Types;

namespace Oglasnik.Models {

    /// <summary>
    /// Svaka komponenta ima ovu klasu kao zaglavlje
    /// </summary>
    [Table("oglasi")]
    public class Oglasi {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name = "Cijena")]
        public decimal cijena { get; set; }

        [Display(Name = "Naslov")]
        public string naslov { get; set; }

        [Display(Name = "Opis")]
        [DataType(DataType.MultilineText)]
        public string opis { get; set; }

        [Display(Name="Datum objave")]
        public DateTime datum_objave { get; set; }

        public string korisnik_id { get; set; }

        public string tip_oglasa { get; set; }
        
    }
    //kraj modela oglasi

    /// <summary>
    /// Bazna klasa za komponente, pošto sve imaju zajednička polja definirana ovdje.
    /// Također omogućava da se komponente dohvaćaju dinamički iz modela Oglas.
    /// </summary>
    public abstract class KomponentaBase {

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int oglas_id { get; set; }

        [Display(Name = "Proizvođač")]
        public string proizvodac { get; set; }

        [Display(Name = "Model")]
        public string model { get; set; }

        [NotMapped]
        public Oglasi oglas {
            get {
                using (OglasnikDbContext db = new OglasnikDbContext()) {
                    return db.Oglasi.Find(oglas_id);
                }
            }
        }
    }

    /// <summary>
    /// Modeli za komponente
    /// </summary>
    #region Modeli za komponente

    [Table("graficke")]
    public class Graficke : KomponentaBase {

        [Display(Name = "Memorija")]
        public string memorija { get; set; }

        [Display(Name = "Slot")]
        public string slot { get; set; }

        [Display(Name = "Frekvencija memorije")]
        public string frek_memorije { get; set; }

        [Display(Name = "Frekvencija jezgre")]
        public string frek_jezgre { get; set; }

        [Display(Name = "Potrošnja")]
        public string potrosnja { get; set; }

    }

    [Table("maticne")]
    public class Maticne : KomponentaBase {

        [Display(Name = "Socket")]
        public string socket { get; set; }

        [Display(Name = "Potrošnja")]
        public string potrosnja { get; set; }

        [Display(Name = "Memorija")]
        public string memorija { get; set; }

    }

    [Table("memorije")]
    public class Memorije : KomponentaBase {

        [Display(Name = "Tip")]
        public string tip { get; set; }

        [Display(Name = "Frekvencija")]
        public string frekvencija { get; set; }

        [Display(Name = "Veličina")]
        public string velicina { get; set; }

    }

    [Table("pohrana")]
    public class Pohrana : KomponentaBase {

        [Display(Name = "Okretaja/min")]
        public string rpm { get; set; }

        [Display(Name = "Sata verzija")]
        public string sata_ver { get; set; }

        [Display(Name = "Veličina")]
        public string velicina { get; set; }

        [Display(Name = "Brzina čitanja")]
        public string brzina_cit { get; set; }

        [Display(Name = "Brzina pisanja")]
        public string brzina_pis { get; set; }

    }

    [Table("procesori")]
    public class Procesori : KomponentaBase {

        [Display(Name = "Broj jezgri")]
        public string jezgre { get; set; }

        [Display(Name = "Socket")]
        public string socket { get; set; }

        [Display(Name = "Frekvencija")]
        public string frekvencija { get; set; }

        [Display(Name = "Potrošnja")]
        public string potrosnja { get; set; }

    }

    [Table("psu")]
    public class Psu : KomponentaBase {

        [Display(Name = "Snaga")]
        public string snaga { get; set; }

    }

    #endregion Modeli za komponente
}
