﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Newtonsoft.Json;
using System.Data.Entity;
using Oglasnik.Models;

namespace Oglasnik.Helpers {
    public static class HtmlHelperSpecFilterExtension {
        public static MvcHtmlString SpecFilter<TModel, TValue>(this HtmlHelper<System.Collections.Generic.IEnumerable<TModel>> html, IEnumerable<string> items, Expression<Func<TModel, TValue>> expression) {
            string displayName = html.DisplayNameFor(expression).ToString();

            string filters = string.Format("<label>{0}</label><div class=\"filters\" data-filter_for=\"{1}\">", displayName, expression.Body.ToString().Replace(expression.Parameters[0].Name + ".", ""));

            foreach (string item in items) {
                filters += string.Format("<div class=\"checkbox\"><label><input type=\"checkbox\" checked />{0}</label></div>", item);
            }

            filters += "</div>";

            return MvcHtmlString.Create(filters);
        }

    }
}
