-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.6.21 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for oglasnik
DROP DATABASE IF EXISTS `oglasnik`;
CREATE DATABASE IF NOT EXISTS `oglasnik` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `oglasnik`;


-- Dumping structure for table oglasnik.graficke
DROP TABLE IF EXISTS `graficke`;
CREATE TABLE IF NOT EXISTS `graficke` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `oglas_id` int(10) unsigned NOT NULL,
  `proizvodac` varchar(50) NOT NULL,
  `model` varchar(50) DEFAULT NULL,
  `memorija` varchar(50) DEFAULT NULL,
  `slot` varchar(50) DEFAULT NULL,
  `frek_memorije` varchar(50) DEFAULT NULL,
  `frek_jezgre` varchar(50) DEFAULT NULL,
  `potrosnja` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_oglasi_graficke_idx` (`oglas_id`),
  CONSTRAINT `FK_oglasi_graficke` FOREIGN KEY (`oglas_id`) REFERENCES `oglasi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table oglasnik.maticne
DROP TABLE IF EXISTS `maticne`;
CREATE TABLE IF NOT EXISTS `maticne` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `oglas_id` int(10) unsigned NOT NULL,
  `proizvodac` varchar(50) NOT NULL,
  `model` varchar(50) DEFAULT NULL,
  `socket` varchar(50) DEFAULT NULL,
  `potrosnja` varchar(50) DEFAULT NULL,
  `memorija` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_oglasi_maticne_idx` (`oglas_id`),
  CONSTRAINT `FK_oglasi_maticne` FOREIGN KEY (`oglas_id`) REFERENCES `oglasi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table oglasnik.memorije
DROP TABLE IF EXISTS `memorije`;
CREATE TABLE IF NOT EXISTS `memorije` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `oglas_id` int(10) unsigned NOT NULL,
  `proizvodac` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `tip` varchar(50) NOT NULL,
  `frekvencija` varchar(50) NOT NULL,
  `velicina` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_oglasi_memorije_idx` (`oglas_id`),
  CONSTRAINT `FK_oglasi_memorije` FOREIGN KEY (`oglas_id`) REFERENCES `oglasi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table oglasnik.oglasi
DROP TABLE IF EXISTS `oglasi`;
CREATE TABLE IF NOT EXISTS `oglasi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cijena` decimal(8,2) NOT NULL,
  `naslov` varchar(50) NOT NULL,
  `opis` varchar(250) NOT NULL,
  `datum_objave` datetime NOT NULL,
  `korisnik_id` varchar(50) NOT NULL,
  `tip_oglasa` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table oglasnik.pohrana
DROP TABLE IF EXISTS `pohrana`;
CREATE TABLE IF NOT EXISTS `pohrana` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `oglas_id` int(10) unsigned NOT NULL,
  `proizvodac` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `rpm` varchar(50) NOT NULL,
  `sata_ver` varchar(50) NOT NULL,
  `velicina` varchar(50) NOT NULL,
  `brzina_cit` varchar(50) NOT NULL,
  `brzina_pis` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_oglasi_pohrana_idx` (`oglas_id`),
  CONSTRAINT `FK_oglasi_pohrana` FOREIGN KEY (`oglas_id`) REFERENCES `oglasi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table oglasnik.procesori
DROP TABLE IF EXISTS `procesori`;
CREATE TABLE IF NOT EXISTS `procesori` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `oglas_id` int(10) unsigned NOT NULL,
  `proizvodac` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `jezgre` varchar(50) NOT NULL,
  `socket` varchar(50) NOT NULL,
  `frekvencija` varchar(50) NOT NULL,
  `potrosnja` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_oglasi_procesori_idx` (`oglas_id`),
  CONSTRAINT `FK_oglasi_procesori` FOREIGN KEY (`oglas_id`) REFERENCES `oglasi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table oglasnik.psu
DROP TABLE IF EXISTS `psu`;
CREATE TABLE IF NOT EXISTS `psu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `oglas_id` int(10) unsigned NOT NULL,
  `proizvodac` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `snaga` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_oglasi_psu_idx` (`oglas_id`),
  CONSTRAINT `FK_oglasi_psu` FOREIGN KEY (`oglas_id`) REFERENCES `oglasi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
